<?php
class Ape extends Animal
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->legs = 2; // Ape (Kera) memiliki 2 kaki
    }

    public function yell()
    {
        return "Auooo";
    }
}
?>
